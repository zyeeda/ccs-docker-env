# 准备工作 #

1. 安装 [docker-machine](https://docs.docker.com/machine/)
2. 安装 [docker-compose](https://docs.docker.com/compose/)
3. 运行 `docker-machine create -d virtualbox ccs` 创建包含 docker 运行环境的虚拟机
4. 运行 `$(docker-machine env ccs)` 让后来的 docker 命令访问此虚拟机
5. 运行 `docker-machine ip` 命令，记录下显示的 IP 地址
6. Maven 的存储库要放置到默认位置（`~/.m2`）

# 如何运行系统 #

1. `git clone https://bitbucket.org/zyeeda/ccs-docker-env` 克隆此工作区
2. `cd ccs-docker-env` 切换到此工作区目录下
3. `docker-compose up -d` 运行系统
4. 访问 `https://<IP ADDRESS>/ccs`

可以直接使用 MySQL 客户端访问 <IP ADDRESS> 3306 端口来查看数据库。

# 如何编译 #

以下命令均在 ccs-docker-env 目录下运行。

## 编译 origin ##

`docker-compose run origin mvn clean install`

## 编译 runtime ##

`docker-compose run runtime mvn clean install`

## 编译 colorvest ##

`docker-compose run colorvest make clean compile`

## 编译 server ##

`docker-compose run server mvn clean package`